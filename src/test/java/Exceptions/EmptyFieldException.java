package Exceptions;

public class EmptyFieldException extends Exception{
    public EmptyFieldException(){}
    public EmptyFieldException(String msg){
        super(msg);
    }
}

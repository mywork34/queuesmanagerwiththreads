package Exceptions;

public class MinGreaterException extends Exception{
    public MinGreaterException(){};
    public MinGreaterException(String msg){
        super(msg);
    }
}

package Model;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable{
    BlockingQueue<Task> tasks;
    AtomicInteger waitingTime;

    public Server(int maxClients){
        this.waitingTime = new AtomicInteger();
        this.tasks = new ArrayBlockingQueue<>(maxClients);
    }

    public void addTask(Task newTask) throws InterruptedException {
        tasks.put(newTask);
        waitingTime.getAndAdd(newTask.getServiceTime());
    }

    public int totalWaitingTime(){
        int sum = 0;
        for(Task t:tasks){
            sum+=t.getServiceTime();
        }
        return sum;
    }

    @Override
    public void run() {
        while(true){
            try {
                Task t = tasks.peek();
                if(t != null) {
                    Thread.sleep(t.getServiceTime() * 1000);
                    waitingTime.getAndAdd(-t.getServiceTime());
                    tasks.take();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public BlockingQueue<Task> getTasks() {
        return tasks;
    }

    public AtomicInteger getWaitingTime(){
        return waitingTime;
    }
}

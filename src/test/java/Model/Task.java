package Model;

public class Task implements Comparable<Task>{
    private int ID;
    private int arrivalTime;
    private int serviceTime;

    public Task(int ID,int arrivalTime,int serviceTime){
        this.arrivalTime = arrivalTime;
        this.ID = ID;
        this.serviceTime = serviceTime;
    }

    public void decreaseServiceTime(){
        this.serviceTime--;
    }

    public int getID() {
        return ID;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    @Override
    public int compareTo(Task o) {
        return this.getArrivalTime()-o.getArrivalTime();
    }

    @Override
    public String toString(){
        return "("+ID+","+arrivalTime+","+serviceTime+")";
    }
}

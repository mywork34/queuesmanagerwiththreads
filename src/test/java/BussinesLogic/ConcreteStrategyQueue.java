package BussinesLogic;

import Model.Server;
import Model.Task;

import java.util.List;

public class ConcreteStrategyQueue implements Strategy{
    @Override
    public void addTask(List<Server> servers, Task t) throws InterruptedException {
        int minLength = servers.get(0).getTasks().size();
        int index = 0;
        int minIndex = 0;
        for (Server s:servers){
            if(s.getTasks().size() < minLength) {
                minLength = s.getWaitingTime().get();
                minIndex = index;
            }
            index++;
        }
        servers.get(minIndex).addTask(t);
    }
}

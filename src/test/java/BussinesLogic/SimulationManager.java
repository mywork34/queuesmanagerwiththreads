package BussinesLogic;

import Exceptions.EmptyFieldException;
import Exceptions.MinGreaterException;
import GUI.AppInputView;
import GUI.AppLogView;
import Model.Server;
import Model.Task;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.System.exit;

public class SimulationManager implements Runnable{
    public int simulationTime;
    public int minServiceTime;
    public int maxServiceTime;
    public int minArrivalTime;
    public int maxArrivalTime;
    public int noClients;
    public int noServers;
    public int peakHour;
    public AppLogView logView;

    public SelectionPolicy policy = SelectionPolicy.SHORTEST_QUEUE;
    private Scheduler scheduler;
    private List<Task> generatedTasks;
    private static Thread t;
    private BufferedWriter bw;


    public SimulationManager() throws IOException {
        AppInputView frame = new AppInputView();
        bw = new BufferedWriter(new FileWriter("Log.txt"));

        frame.getStartSimulation().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if(frame.hasEmpty() != 0){
                    try {
                        throw new EmptyFieldException("Empty Field!");
                    } catch (EmptyFieldException ex) {
                        JOptionPane.showMessageDialog(frame,"Trebuie sa completati toate campurile!");
                    }

                }
                else {
                    logView = new AppLogView(noServers + 2);
                    simulationTime = Integer.parseInt(frame.getSimtime());
                    minServiceTime = Integer.parseInt(frame.getMinService());
                    maxServiceTime = Integer.parseInt(frame.getMaxService());
                    minArrivalTime = Integer.parseInt(frame.getMinArrival());
                    maxArrivalTime = Integer.parseInt(frame.getMaxArrival());
                    if(minArrivalTime > maxArrivalTime || minServiceTime >maxServiceTime)
                        try {
                            throw new MinGreaterException("The min value is greater!");
                        } catch (MinGreaterException ex) {
                            JOptionPane.showMessageDialog(frame,"The min value is greater than the max!");
                        }
                    noClients = Integer.parseInt(frame.getNoClients());
                    noServers = Integer.parseInt(frame.getNoQues());

                    scheduler = new Scheduler(noServers, noClients);
                    scheduler.changeStrategy(policy);
                    generatedTasks = generateNRandomTasks();
                    t.start();
                }
            }
        });

    }

    private List<Task> generateNRandomTasks(){
        List<Task> rez = new ArrayList<>();
        for(int i = 1; i<=noClients;i++){
            int ATime = minArrivalTime + (int)(Math.random()*(maxArrivalTime-minArrivalTime+1));
            int STime = minServiceTime + (int)(Math.random()*(maxServiceTime-minServiceTime+1));
            Task t = new Task(i,ATime,STime);
            rez.add(t);
        }
        Collections.sort(rez);
        return rez;
    }

    void printLog(int time) throws IOException {
        bw.write("Time: "+time+"\n");
        bw.flush();
        String str = "Waiting clients: ";
        for(Task t:generatedTasks){
            str = str+t.toString()+";";
        }
        bw.write(str +"\n");
        String logCurrent = "Time: "+time+"\nWaiting clients: "+str+"\n";
        bw.flush();
        List<Server> serverList = scheduler.getServers();
        int i = 1;
        for(Server s:serverList){
            String strQ = "Queue "+i+": ";
            if (s.getTasks().size() == 0)
                strQ = strQ + "closed";
            else{
                for(Task t:s.getTasks()){
                    strQ = strQ + t.toString()+";";
                }
            }
            logCurrent = logCurrent+strQ+"\n";
            bw.write(strQ+"\n");
            bw.flush();
            i++;
        }
        logView.setLogCurrent(logCurrent);
    }

    private void printLogInfo(float avgServiceTime,float avgWaitingTime,int PeakHour,int maxClientsOnQueue) throws IOException {
        String s = "Average service time: "+avgServiceTime+"\n" +"Average waiting time: "+ avgWaitingTime+"\n"+"Peak hour: "+ PeakHour+"  (With "+maxClientsOnQueue+ " clients waiting on queues)\n";
        logView.setLogCurrent(logView.getLogCurrent()+s);
        bw.write("Average service time: "+avgServiceTime+"\n");
        bw.write("Average waiting time: "+ avgWaitingTime+"\n");
        bw.write("Peak hour: "+ PeakHour+"  (With "+maxClientsOnQueue+ " clients waiting on queues)\n");
    }

    private float getAvaerageServiceTime(){
        float rez =0;
        for(Task t:generatedTasks){
            rez+=t.getServiceTime();
        }
        rez = rez/generatedTasks.size();
        return rez;
    }

    private float decrementTasks() throws InterruptedException {
        List<Server> serverList = scheduler.getServers();
        float rez = 0;
        for(Server s:serverList){
            rez += s.totalWaitingTime();
            if(s.getTasks().size() > 0){
                s.getTasks().peek().decreaseServiceTime();
            }
        }
        return rez;
    }

    public static void main(String[] args) throws IOException {
        SimulationManager sim = new SimulationManager();
        t = new Thread(sim);
    }

    @Override
    public void run() {
        int currentTime = 0;
        float avgWaitingTime = 0;
        int PeakHour = 0;
        int maxClientsOnQueues = 0;
        float avgServiceTime = getAvaerageServiceTime();
        while(currentTime<=simulationTime && (generatedTasks.size()>0 || scheduler.serversOpen() == 1)){
            Task t;
            while(generatedTasks.size() > 0 && generatedTasks.get(0).getArrivalTime() == currentTime ) {
                t = generatedTasks.get(0);
                generatedTasks.remove(0);
                try {
                    scheduler.dispatchTask(t);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
            if(scheduler.getMaxClientsOnQueues() > maxClientsOnQueues){
                PeakHour = currentTime;
                maxClientsOnQueues = scheduler.getMaxClientsOnQueues();
            }
            try {

                printLog(currentTime);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                avgWaitingTime += decrementTasks()/noServers;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            currentTime++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            printLog(currentTime);
            printLogInfo(avgServiceTime,avgWaitingTime/currentTime,PeakHour,maxClientsOnQueues);
            bw.flush();
            bw.close();
            Thread.sleep(1000*10);
            exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}

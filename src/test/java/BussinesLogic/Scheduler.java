package BussinesLogic;

import Model.Server;
import Model.Task;

import java.util.ArrayList;
import java.util.List;

public class Scheduler {
    private List<Server> servers;
    private int maxNoServers;
    private int maxTasksPerServer;
    private Strategy strategy;

    public Scheduler(int maxNoServers,int maxTasksPerServer){
        servers = new ArrayList<Server>();
        for(int i=0;i<maxNoServers;i++){
            Server s = new Server(maxTasksPerServer);
            servers.add(s);
            Thread t = new Thread(s);
            t.start();
        }
    }

    public int serversOpen(){
        for(Server s:servers){
            if(s.totalWaitingTime() > 0)
                return 1;
        }
        return 0;
    }

    public int getMaxClientsOnQueues(){
        int no =0;
        for(Server s:servers){
            no+= s.getTasks().size();
        }
        return no;
    }

    public float getAverageWaitingTime(){
        float rez =0;
        for(Server s:servers){
            rez += s.totalWaitingTime();
        }
        if(rez != 0)
            rez = rez/maxNoServers;
        return rez;
    }

    public void changeStrategy(SelectionPolicy policy){
        if(policy == SelectionPolicy.SHORTEST_QUEUE){
            strategy = new ConcreteStrategyQueue();
        }
        if(policy == SelectionPolicy.SHORTEST_TIME){
            strategy = new ConcreteStrategyTime();
        }
    }

    public void dispatchTask(Task t) throws InterruptedException {
        strategy.addTask(servers,t);
    }

    public List<Server> getServers() {
        return servers;
    }
}

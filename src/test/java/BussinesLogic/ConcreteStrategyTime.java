package BussinesLogic;

import Model.Server;
import Model.Task;

import java.util.List;

public class ConcreteStrategyTime implements Strategy{

    @Override
    public void addTask(List<Server> servers, Task t) throws InterruptedException {
        int minWaitingTime = servers.get(0).totalWaitingTime();
        int index = 0;
        int minIndex = 0;
        for (Server s:servers){
            if(s.totalWaitingTime() < minWaitingTime) {
                minWaitingTime = s.totalWaitingTime();
                minIndex = index;
            }
            index++;
        }
        servers.get(minIndex).addTask(t);
    }
}

package GUI;

import javax.swing.*;

public class AppLogView extends JFrame{
    private JLabel logMsg = new JLabel("Simulation log");
    private JLabel currentLabel = new JLabel("Current log:");
    private JLabel previousLabel = new JLabel("Previous log:");
    private JTextPane logCurrent;
    private JTextPane logPrecedent;

    public AppLogView(int height){
        this.setBounds(300,100,535,600 );
        this.setTitle("SimulationLOG");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.getContentPane().setLayout(null);

        logMsg.setBounds(200,10,200,20);
        this.getContentPane().add(logMsg);

        logCurrent = new JTextPane();
        logCurrent.setBounds(10,40,500,500);
        logCurrent.setText("NULL");
        this.getContentPane().add(logCurrent);


        this.setVisible(true);
    }

    public String getLogCurrent(){
        return logCurrent.getText();
    }
    public void setLogCurrent(String msg){
        logCurrent.setText(msg);
    }


}

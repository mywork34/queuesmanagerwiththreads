package GUI;

import javax.swing.*;

public class AppInputView extends JFrame {
    private JLabel no_clients = new JLabel("Number of clients: ");
    private JTextField in_clients;
    private JLabel no_ques = new JLabel("Number of ques: ");
    private JTextField in_ques;
    private JLabel simtime = new JLabel("Simulation time: ");
    private JTextField in_simtime;
    private JLabel arrival_timeMin = new JLabel("Arrival time (min): ");
    private JLabel arrival_timeMax = new JLabel("Arrival time (max): ");
    private JTextField in_minATime;
    private JTextField in_maxATime;
    private JLabel service_timeMin = new JLabel("Service time (min): ");
    private JLabel service_timeMax = new JLabel("Service time (max): ");
    private JTextField in_minSTime;
    private JTextField in_maxSTime;
    private JButton startSimulation;

    public AppInputView(){
        this.setBounds(100,100,280,300);
        this.setTitle("SimulationInput");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.getContentPane().setLayout(null);

        no_clients.setBounds(10,10,120,20);
        this.getContentPane().add(no_clients);

        in_clients = new JTextField();
        in_clients.setBounds(120,10,120,20);
        this.getContentPane().add(in_clients);

        no_ques.setBounds(20,40,120,20);
        this.getContentPane().add(no_ques);

        in_ques = new JTextField();
        in_ques.setBounds(120,40,120,20);
        this.getContentPane().add(in_ques);

        simtime.setBounds(25,70,120,20);
        this.getContentPane().add(simtime);

        in_simtime = new JTextField();
        in_simtime.setBounds(120,70,120,20);
        this.getContentPane().add(in_simtime);

        arrival_timeMin.setBounds(15,100,120,20);
        this.getContentPane().add(arrival_timeMin);

        in_minATime = new JTextField();
        in_minATime.setBounds(120,100,120,20);
        this.getContentPane().add(in_minATime);

        arrival_timeMax.setBounds(15,130,120,20);
        this.getContentPane().add(arrival_timeMax);

        in_maxATime = new JTextField();
        in_maxATime.setBounds(120,130,120,20);
        this.getContentPane().add(in_maxATime);

        service_timeMin.setBounds(10,160,120,20);
        this.getContentPane().add(service_timeMin);

        in_minSTime = new JTextField();
        in_minSTime.setBounds(120,160,120,20);
        this.getContentPane().add(in_minSTime);

        service_timeMax.setBounds(10,190,120,20);
        this.getContentPane().add(service_timeMax);

        in_maxSTime = new JTextField();
        in_maxSTime.setBounds(120,190,120,20);
        this.getContentPane().add(in_maxSTime);

        startSimulation = new JButton("START SIMULATION");
        startSimulation.setBounds(40,220,170,20);
        this.getContentPane().add(startSimulation);

        this.setVisible(true);
    }

    public JButton getStartSimulation(){
        return startSimulation;
    }

    public String getNoClients(){
        return in_clients.getText();
    }

    public String getNoQues(){
        return in_ques.getText();
    }

    public String getSimtime(){
        return in_simtime.getText();
    }

    public String getMinArrival(){
        return in_minATime.getText();
    }

    public String getMaxArrival(){
        return in_maxATime.getText();
    }

    public String getMinService(){
        return in_minSTime.getText();
    }

    public String getMaxService(){
        return in_maxSTime.getText();
    }

    public int hasEmpty(){
        if(in_clients.getText().isEmpty()){
            return 1;
        }
        if(in_ques.getText().isEmpty()){
            return 2;
        }
        if(in_simtime.getText().isEmpty()){
            return 3;
        }
        if(in_minATime.getText().isEmpty()){
            return 4;
        }
        if(in_maxATime.getText().isEmpty()){
            return 5;
        }
        if(in_minSTime.getText().isEmpty()){
            return 6;
        }
        if(in_maxSTime.getText().isEmpty()){
            return 7;
        }
        return 0;
    }
}
